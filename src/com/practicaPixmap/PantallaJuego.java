package com.practicaPixmap;

import java.util.Timer;

import android.text.TextWatcher;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.pacman.elementos.*;

public class PantallaJuego implements Screen {
	
	private JuegoPrincipal juego;
	//private Texture texturaCasilla, texturaPacman;
	//private Sprite casillaSprite, pacmanSprite;
	private SpriteBatch batcher;
	private static final int        PIXELES = 32;
	private static final int        FRAME_COLS = 4;         // #1
    private static final int        FRAME_ROWS = 4;         // #2
    private static final int 		posPacmanX = 7*PIXELES;
    private static final int 		posPacmanY = 6*PIXELES;
    
    private static final float		frameAnimacion = 0.05f;

    private Animation                       pacmanAnimationR;          // #3
    private Animation                       pacmanAnimationU;
    private Animation                       pacmanAnimationL;
    private Animation                       pacmanAnimationD;
    private Texture                         pacmanTexture;              // #4
    private TextureRegion[]                 pacmanTextureR;
    private TextureRegion[]                 pacmanTextureU;
    private TextureRegion[]                 pacmanTextureL;
    private TextureRegion[]                 pacmanTextureD;			// #5
    private TextureRegion                   pacmanTextureRegion;           // #7
    private Texture dPadTexture;
    private Texture dPadTextureU;
    private Texture dPadTextureL;
    private Texture dPadTextureR;
    private Texture dPadTextureD;
    private Sprite dPadSrite;
    
    private int anchoDPad=0, altoDpad=0;
    private String direccionP="derecha", tmpDireccionP ="";
    private String direccionF="derecha", tmpDireccionF ="";
    private int portal100X = 1 * PIXELES;
    private int portal100Y = 16 * PIXELES;
    private int portal101X = 18 * PIXELES;
    private int portal101Y = 16 * PIXELES;
    private int portal200X = 1 * PIXELES;
    private int portal200Y = 7 * PIXELES;
    private int portal201X = 18 * PIXELES;
    private int portal201Y = 7 * PIXELES;
    private int contadorGalleta = 0;
    private boolean fruta = true;
    
    private float stateTime;                                        // #8
    
    //CASILLAS
    private Casillas casillaClase = new Casillas(1);
    private int[][] Laberinto;
    private int[][] laberintoOrigin;
    private int[] tmpPosPacman = new int[2]; //guarda posiciones devuelta por el metodo de mover personajes
    private int[] tmpPosFantasma = new int[2]; //guarda posiciones devuelta por el metodo de mover personajes
    private Texture[][] texturaCasillas;
    private Sprite[][] casillasSprite;
    private int numCol, numFilas;
    private static int SCREEN_WIDTH = 800;
    private static int SCREEN_HEIGHT = 480;
    private static int FRAMES_PACMAN = 8;
    private static int FRAMES_FANTASMA = 4;
 
	//things needed to draw
    //private Pixmap pixmap2;
    //private Texture texturaMov;
    private Pacman pacman;
    private int ny=-1;
	private int nx=-1;
	private Texture texturaX;
	private Sprite spriteX;
	
	//Bitmap
	private BitmapFont pacX;
	private BitmapFont pacY;
	private BitmapFont nxT;
	private BitmapFont nyT;
	private BitmapFont espacioX;
	private BitmapFont espacioY;
	private BitmapFont bloqueX;
	private BitmapFont bloqueY;
	private BitmapFont puntajeBitmap;
	private int puntaje = 0;
	private BitmapFont vidasBitmap;
	private int vidas = 5;
	private BitmapFont nivelBitmap;
	private String nivel = "Nivel 1";
	private BitmapFont cronometroBitmap;
	private int crono = 0;
	private boolean gameOver = false;
	private Cronometro cronoHilo;
	
	//Fantasmas
	private static final int 		posFantasmaRoX = 7*PIXELES;
    private static final int 		posFantasmaRoY = 12*PIXELES;
    private static final int 		posFantasmaRosaX = 10*PIXELES;
    private static final int 		posFantasmaRosaY = 12*PIXELES;
    private static final int 		posFantasmaCX = 7*PIXELES;
    private static final int 		posFantasmaCY = 15*PIXELES;
    private static final int 		posFantasmaNX = 10*PIXELES;
    private static final int 		posFantasmaNY = 15*PIXELES;
    
	private Texture fantasmaRojoTexture;
	private Sprite fantasmaRojoSprite;
	private Fantasma fantasmaRojo;
	
	private Texture fantasmaNaranjaTexture;
	private Sprite fantasmaNaranjaSprite;
	private Fantasma fantasmaNaranja;
	
	private Texture fantasmaCelesteTexture;
	private Sprite fantasmaCelesteSprite;
	private Fantasma fantasmaCeleste;
	
	private Texture fantasmaRosaTexture;
	private Sprite fantasmaRosaSprite;
	private Fantasma fantasmaRosa;
	
	private boolean fantasmasPowerUp = false;
	private boolean fantasmasRoAzul = false;
	private boolean fantasmasRosaAzul = false;
	private boolean fantasmasCAzul = false;
	private boolean fantasmasNAzul = false;
	private Tiempo tiempoAzul;
	private int contadorAzul = 0;
	
	public PantallaJuego(JuegoPrincipal game) {
		this.juego = game;
	}
	@Override
	public void dispose() {
		Gdx.app.log("MyLibGDXGame", "Game.dispose()");
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		pacmanTexture.dispose();
		for (int i = 0; i < numFilas; i++) {
	        for (int j = 0; j < numCol; j++) {
	        	texturaCasillas[i][j].dispose();
	        }
		}
		Gdx.app.log("MyLibGDXGame", "Game.pause()");
		//texturaMov.dispose();
		//pixmap2.dispose();
		pacX.dispose();
		pacY.dispose();
		nxT.dispose();
		nyT.dispose();
		dPadTexture.dispose();
		
		batcher.dispose();
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT | GL10.GL_STENCIL_BUFFER_BIT);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT); 
		
		stateTime += Gdx.graphics.getDeltaTime();
		
		batcher.begin();
		
		//MOVER TOTALMENTE CON EL TOUCH
		if(Gdx.input.isTouched()){
			nx = Gdx.input.getX();
			ny = (SCREEN_HEIGHT - Gdx.input.getY());
		}
		
		if(contadorGalleta > 0 && !gameOver){
			tmpPosPacman = movimientoPersonajeDPad(pacman.getX(), pacman.getY());
			pacman.setX(tmpPosPacman[0]);
			pacman.setY(tmpPosPacman[1]);
			//Movimiento Fantasma Rojo
			tmpPosFantasma = movimientoFantasma(fantasmaRojo.getX(), fantasmaRojo.getY(), pacman.getX(), pacman.getY());
			fantasmaRojo.setX(tmpPosFantasma[0]);
			fantasmaRojo.setY(tmpPosFantasma[1]);
			//Movimiento Fantasma Rosa
			tmpPosFantasma = movimientoFantasma(fantasmaRosa.getX(), fantasmaRosa.getY(), pacman.getX(), pacman.getY());
			fantasmaRosa.setX(tmpPosFantasma[0]);
			fantasmaRosa.setY(tmpPosFantasma[1]);
			//Movimiento Fantasma Celeste
			tmpPosFantasma = movimientoFantasma(fantasmaCeleste.getX(), fantasmaCeleste.getY(), pacman.getX(), pacman.getY());
			fantasmaCeleste.setX(tmpPosFantasma[0]);
			fantasmaCeleste.setY(tmpPosFantasma[1]);
			//Movimiento Fantasma Naranja
			tmpPosFantasma = movimientoFantasma(fantasmaNaranja.getX(), fantasmaNaranja.getY(), pacman.getX(), pacman.getY());
			fantasmaNaranja.setX(tmpPosFantasma[0]);
			fantasmaNaranja.setY(tmpPosFantasma[1]);
		}
		/*
		pacX.draw(batcher, "pacX: "+pacman.getX(), SCREEN_WIDTH-100, 300);
		espacioX.draw(batcher, "FanX: "+fantasmaRojo.getX(), SCREEN_WIDTH-100, 275);
		pacY.draw(batcher, "pacY: "+pacman.getY(), SCREEN_WIDTH-100, 250);
		espacioY.draw(batcher, "FanY: "+fantasmaRojo.getY(), SCREEN_WIDTH-100, 225);
		nxT.draw(batcher, "Proxima D: "+tmpDireccionF, SCREEN_WIDTH-150, 200);
		nyT.draw(batcher, "Direccion: "+direccionF, SCREEN_WIDTH-150, 175);
		*/
		puntajeBitmap.draw(batcher, "Score: "+puntaje, anchoDPad+PIXELES, SCREEN_HEIGHT/2+5*PIXELES);
		nivelBitmap.draw(batcher, "Nivel: "+nivel, anchoDPad+PIXELES, SCREEN_HEIGHT/2+4*PIXELES);
		cronometroBitmap.draw(batcher, "Time: "+crono, anchoDPad+PIXELES, SCREEN_HEIGHT/2+3*PIXELES);
		if(vidas != 0){
			vidasBitmap.draw(batcher, "Vidas: "+vidas, anchoDPad+PIXELES, SCREEN_HEIGHT/2+1*PIXELES);
			if(contadorGalleta <= 0){
				cronoHilo = null;
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} else{
			gameOver = true;
			vidasBitmap.draw(batcher, "GAME OVER", anchoDPad+PIXELES, SCREEN_HEIGHT/2+1*PIXELES);
			cronoHilo = null;
		}
		/*espacioX.draw(batcher, ""+pacman.getX(), 125, 160);
		espacioY.draw(batcher, ""+pacman.getX(), 150, 160);
		bloqueX.draw(batcher, ""+pacman.getX(), 175, 160);
		bloqueY.draw(batcher, ""+pacman.getX(), 200, 160);
		*/
		batcher.draw(dPadSrite, anchoDPad,altoDpad);
		//batcher.draw(spriteX, nx, ny,pacman.getWidth(),pacman.getHeight());
		batcher.draw(pacmanTextureRegion, pacman.getX(), pacman.getY(),pacman.getWidth(),pacman.getHeight());
		batcher.draw(fantasmaRojoSprite, fantasmaRojo.getX(), fantasmaRojo.getY(),fantasmaRojo.getWidth(),fantasmaRojo.getHeight());
		batcher.draw(fantasmaNaranjaSprite, fantasmaNaranja.getX(), fantasmaNaranja.getY(),fantasmaNaranja.getWidth(),fantasmaNaranja.getHeight());
		batcher.draw(fantasmaCelesteSprite, fantasmaCeleste.getX(), fantasmaCeleste.getY(),fantasmaCeleste.getWidth(),fantasmaCeleste.getHeight());
		batcher.draw(fantasmaRosaSprite, fantasmaRosa.getX(), fantasmaRosa.getY(),fantasmaRosa.getWidth(),fantasmaRosa.getHeight());
		
		comer();
		muerte();
        for (int i = 0; i < Laberinto.length; i++) {
            for (int j = 0; j < Laberinto[i].length; j++) {
            	casillasSprite[i][j].draw(batcher);
            	
            }
        }
        
        
        
        
		batcher.end();
	}

	
	private void comer() {
		int pi, pj, gx, gy, px, py;
		pi = laberintoOrigin.length-(pacman.getY() / PIXELES)-1;
		pj = ((pacman.getX() / PIXELES));
		px = pacman.getX() + (PIXELES/2);
		py = pacman.getY() + (PIXELES/2);
		gx = (PIXELES*pj)+(PIXELES/2);
		gy = (PIXELES*(laberintoOrigin.length - 1 - pi))+(PIXELES/2);
		if(laberintoOrigin[pi][pj] == 0 || laberintoOrigin[pi][pj] == 11 || laberintoOrigin[pi][pj] == 5 
				|| laberintoOrigin[pi][pj] == 100 || laberintoOrigin[pi][pj] == 101 || laberintoOrigin[pi][pj] == 200 || laberintoOrigin[pi][pj] == 201){
			if(px==gx && py==gy){
				casillasSprite[pj][laberintoOrigin.length-1-pi].setTexture(new Texture("bloquee.png"));
				//casillaClase.setLaberinto(pi, pj, 99);
				/*
				if(puntaje >= 400){
					laberintoOrigin[20][10] = 11;
					if(fruta){
						casillasSprite[10][laberintoOrigin.length-1-20].setTexture(new Texture("fruta.png"));
						
					}
				}*/
				if(laberintoOrigin[pi][pj] == 0){
					puntaje = puntaje + 10;
					laberintoOrigin[pi][pj] = 99;
					contadorGalleta--;
				}else if(laberintoOrigin[pi][pj] == 5){
					puntaje = puntaje + 120;
					contadorGalleta--;
					fantasmaRojoSprite.setTexture(new Texture("phantomAzul.png"));
					fantasmaCelesteSprite.setTexture(new Texture("phantomAzul.png"));
					fantasmaRosaSprite.setTexture(new Texture("phantomAzul.png"));
					fantasmaNaranjaSprite.setTexture(new Texture("phantomAzul.png"));
					
					if(tiempoAzul == null){
						tiempoAzul = new Tiempo();
					}
					
					if(fantasmasPowerUp){
						tiempoAzul = null;
						contadorAzul = 0;
						tiempoAzul = new Tiempo();
						
					}
					fantasmasPowerUp = true;
					fantasmasRoAzul = true;
					fantasmasRosaAzul = true;
					fantasmasCAzul = true;
					fantasmasNAzul = true;
					laberintoOrigin[pi][pj] = 99;
					tiempoAzul.start();
					
				}else if(laberintoOrigin[pi][pj] == 11){
					puntaje = puntaje + 1050;
					laberintoOrigin[pi][pj] = 99;
					fruta = false;
				}else if(laberintoOrigin[pi][pj] == 100){
					pacman.setX(portal101X);
					pacman.setY(portal101Y);
				}else if(laberintoOrigin[pi][pj] == 101){
					pacman.setX(portal100X);
					pacman.setY(portal100Y);
				}else if(laberintoOrigin[pi][pj] == 200){
					pacman.setX(portal201X);
					pacman.setY(portal201Y);
				}else if(laberintoOrigin[pi][pj] == 201){
					pacman.setX(portal200X);
					pacman.setY(portal200Y);
				}
				
			}
		}
		if(fantasmasPowerUp){
			
			
			if(contadorAzul >=5){
				tiempoAzul = null;
				fantasmasPowerUp = false;
				fantasmaRojoSprite.setTexture(new Texture("phantomRojo.png"));
				fantasmaCelesteSprite.setTexture(new Texture("phantomCeleste.png"));
				fantasmaRosaSprite.setTexture(new Texture("phantomRosa.png"));
				fantasmaNaranjaSprite.setTexture(new Texture("phantomNaranja.png"));
				contadorAzul = 0;
			}
		}
		
	}
	
	private void muerte() {
		boolean tocado = false;
		
		if(fantasmaRojo.getX() <= pacman.getX() && (fantasmaRojo.getX()+PIXELES) >= pacman.getX()
			&& fantasmaRojo.getY() <= pacman.getY() && (fantasmaRojo.getY()+PIXELES) >= pacman.getY()){
			if(fantasmasRoAzul){
				puntaje = puntaje + 850;
				fantasmaRojo.setX(posFantasmaRoX);
				fantasmaRojo.setY(posFantasmaRoY);
				fantasmaRojoSprite.setTexture(new Texture("phantomRojo.png"));
				fantasmasRoAzul = false;
			} else{
				regresarPosiciones();
			}
			tocado = true;
		}
		if(fantasmaRosa.getX() <= pacman.getX() && (fantasmaRosa.getX()+PIXELES) >= pacman.getX()
				&& fantasmaRosa.getY() <= pacman.getY() && (fantasmaRosa.getY()+PIXELES) >= pacman.getY()){
			if(fantasmasRosaAzul){
				puntaje = puntaje + 850;
				fantasmaRosa.setX(posFantasmaRosaX);
				fantasmaRosa.setY(posFantasmaRosaY);
				fantasmaRosaSprite.setTexture(new Texture("phantomRosa.png"));
				fantasmasRosaAzul = false;
			} else{
				regresarPosiciones();
			}
			tocado = true;
		}
		if(fantasmaCeleste.getX() <= pacman.getX() && (fantasmaCeleste.getX()+PIXELES) >= pacman.getX()
				&& fantasmaCeleste.getY() <= pacman.getY() && (fantasmaCeleste.getY()+PIXELES) >= pacman.getY()){
			if(fantasmasCAzul){
				puntaje = puntaje + 850;
				fantasmaCeleste.setX(posFantasmaCX);
				fantasmaCeleste.setY(posFantasmaCY);
				fantasmaCelesteSprite.setTexture(new Texture("phantomCeleste.png"));
				fantasmasCAzul = false;
			} else{
				regresarPosiciones();
			}
			tocado = true;
		}
		if(fantasmaNaranja.getX() <= pacman.getX() && (fantasmaNaranja.getX()+PIXELES) >= pacman.getX()
				&& fantasmaNaranja.getY() <= pacman.getY() && (fantasmaNaranja.getY()+PIXELES) >= pacman.getY()){
			if(fantasmasNAzul){
				puntaje = puntaje + 850;
				fantasmaNaranja.setX(posFantasmaNX);
				fantasmaNaranja.setY(posFantasmaNY);
				fantasmaNaranjaSprite.setTexture(new Texture("phantomNaranja.png"));
				fantasmasNAzul = false;
			} else{
				regresarPosiciones();
			}
			tocado = true;
		}
		if(tocado){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void regresarPosiciones(){
		vidas = vidas -1;
		
		pacman.setX(posPacmanX);
		pacman.setY(posPacmanY);
		
		fantasmaCeleste.setX(posFantasmaCX);
		fantasmaCeleste.setY(posFantasmaCY);
		fantasmaRojo.setX(posFantasmaRoX);
		fantasmaRojo.setY(posFantasmaRoY);
		fantasmaRosa.setX(posFantasmaRosaX);
		fantasmaRosa.setY(posFantasmaRosaY);
		fantasmaNaranja.setX(posFantasmaNX);
		fantasmaNaranja.setY(posFantasmaNY);
		direccionF = "derecha";
		direccionP = "derecha";
	}
	
	//Movimento de Pacman con el D-Pad
	private int[] movimientoPersonajeDPad(int posX, int posY){
		int []tmpPos = {posX,posY};
		//Izquierda
		if(a(0)<nx && a(3)>nx && b(3)<ny && b(9)>ny){
			tmpDireccionP = "izquierda";
			dPadSrite.setTexture(dPadTextureL);
		}
		//Arriba
		if(a(3)<nx && a(6)>nx && b(6)<ny && b(9)>ny){
			tmpDireccionP = "arriba";
			dPadSrite.setTexture(dPadTextureU);
		}
		//Derecha
		if(a(6)<nx && a(9)>nx && b(3)<ny && b(6)>ny){
			tmpDireccionP = "derecha";
			dPadSrite.setTexture(dPadTextureR);
		}
		//Abajo
		if(a(3)<nx && a(6)>nx && b(0)<ny && b(3)>ny){
			tmpDireccionP = "abajo";
			dPadSrite.setTexture(dPadTextureD);
		}
		//Arriba
		if(direccionP.equals("arriba")){
			if(a(3)<nx && a(6)>nx && b(0)<ny && b(3)>ny){
				direccionP = "abajo";
			}
			if(tmpDireccionP.equals("derecha")){
				if(espacioDerecha(posX, posY)){
					direccionP = "derecha";
				}
			}
			if(tmpDireccionP.equals("izquierda")){
				if(espacioIzquierda(posX, posY)){
					direccionP = "izquierda";
				}
			}
			if(direccionP.equals("arriba")){
				if(espacioArriba(posX, posY)){
					tmpPos[1] = tmpPos[1] + FRAMES_PACMAN;
				}else{
					dPadSrite.setTexture(dPadTexture);
					
				}
				pacmanTextureRegion = pacmanAnimationU.getKeyFrame(stateTime, true);
			}
		}
		//Abajo
		if(direccionP.equals("abajo")){
			if(a(3)<nx && a(6)>nx && b(6)<ny && b(9)>ny){
				direccionP = "arriba";
			}
			if(tmpDireccionP.equals("derecha")){
				if(espacioDerecha(posX, posY)){
					direccionP = "derecha";
				}
			}
			if(tmpDireccionP.equals("izquierda")){
				if(espacioIzquierda(posX, posY)){
					direccionP = "izquierda";
				}
			}
			if(direccionP.equals("abajo")){
				if(espacioAbajo(posX, posY)){
					tmpPos[1] = tmpPos[1] - FRAMES_PACMAN;
				}else{
					dPadSrite.setTexture(dPadTexture);
					
				}
				pacmanTextureRegion = pacmanAnimationD.getKeyFrame(stateTime, true);
			}
		}
		//Derecha
		if(direccionP.equals("derecha")){
			if(a(0)<nx && a(3)>nx && b(3)<ny && b(6)>ny){
				direccionP = "izquierda";
			}
			if(tmpDireccionP.equals("arriba")){
				if(espacioArriba(posX, posY)){
					direccionP = "arriba";
				}
			}
			if(tmpDireccionP.equals("abajo")){
				if(espacioAbajo(posX, posY)){
					direccionP = "abajo";
				}
			}
			if(direccionP.equals("derecha")){
				if(espacioDerecha(posX, posY)){
					tmpPos[0] = tmpPos[0] + FRAMES_PACMAN;
				}else{
					dPadSrite.setTexture(dPadTexture);
					
				}
				pacmanTextureRegion = pacmanAnimationR.getKeyFrame(stateTime, true);
			}
		}
		//Izquierda
		if(direccionP.equals("izquierda")){
			if(a(6)<nx && a(9)>nx && b(3)<ny && b(6)>ny){
				direccionP = "derecha";
			}
			if(tmpDireccionP.equals("arriba")){
				if(espacioArriba(posX, posY)){
					direccionP = "arriba";
				}
			}
			if(tmpDireccionP.equals("abajo")){
				if(espacioAbajo(posX, posY)){
					direccionP = "abajo";
				}
			}
			if(direccionP.equals("izquierda")){
				if(espacioIzquierda(posX, posY)){
					tmpPos[0] = tmpPos[0] - FRAMES_PACMAN;
				} else{
					dPadSrite.setTexture(dPadTexture);
					
				}
				pacmanTextureRegion = pacmanAnimationL.getKeyFrame(stateTime, true);
			}
		}
		return tmpPos;
	}
	
	//Funcion SOLO para el D-Pad
	private int a(int n){
		return anchoDPad+PIXELES*n;
	}
	//Funcion SOLO para el D-Pad
	private int b(int n){
		return altoDpad+PIXELES*n;
	}
	
	private boolean espacioDerecha(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-(posY / PIXELES)-1;
		cj = ((posX / PIXELES)+1);
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES)-16;//Valor 16 varia segun el telefono
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11
				|| tmpBloqueMatriz == 100 || tmpBloqueMatriz == 101 || tmpBloqueMatriz == 200 || tmpBloqueMatriz == 201)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( tmpBloqueY>=posY && posY>=(tmpBloqueY-PIXELES))
					//&& tmpBloqueY>=(posY+PIXELES) && (posY+PIXELES)>=(tmpBloqueY-PIXELES))
				hayEspacio = true;
		}
		
		return hayEspacio;
	}
	private boolean espacioIzquierda(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-(posY / PIXELES)-1;
		cj = ((posX-FRAMES_PACMAN) / PIXELES);
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES)-16;//Valor 16 varia segun el telefono
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11
				|| tmpBloqueMatriz == 100 || tmpBloqueMatriz == 101 || tmpBloqueMatriz == 200 || tmpBloqueMatriz == 201)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( tmpBloqueY>=posY && posY>=(tmpBloqueY-PIXELES))
					//&& tmpBloqueY>=(posY+PIXELES) && (posY+PIXELES)>=(tmpBloqueY-PIXELES))
				hayEspacio = true;
			if(tmpBloqueMatriz == 0){
				
			}
		}
		
		return hayEspacio;
	}
	private boolean espacioArriba(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-(posY / PIXELES)-1-1;
		cj = ((posX / PIXELES));
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES);
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11
				|| tmpBloqueMatriz == 100 || tmpBloqueMatriz == 101 || tmpBloqueMatriz == 200 || tmpBloqueMatriz == 201)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( (tmpBloqueX)<=posX && posX<=tmpBloqueX+PIXELES
				&& (tmpBloqueX)<=(posX+32) && (posX+32)<=tmpBloqueX+PIXELES)
				hayEspacio = true;
		}
		
		return hayEspacio;
	}
	private boolean espacioAbajo(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-((posY-FRAMES_PACMAN) / PIXELES)-1;
		cj = (posX / PIXELES);
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES);
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11
				|| tmpBloqueMatriz == 100 || tmpBloqueMatriz == 101 || tmpBloqueMatriz == 200 || tmpBloqueMatriz == 201)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( (tmpBloqueX)<=posX && posX<=tmpBloqueX+PIXELES
				&& (tmpBloqueX)<=(posX+32) && (posX+32)<=tmpBloqueX+PIXELES)
				hayEspacio = true;
		}
		
		return hayEspacio;
	}
	
	//Movimiento de los Fantasmas--------------------------------------------------------------------------------------------------
	private int[] movimientoFantasma(int posFantasmaX, int posFantasmaY, int posObjetivoX, int posObjetivoY){
		int []tmpPos = {posFantasmaX,posFantasmaY};
		//Izquierda
		if(posFantasmaX > posObjetivoX && posFantasmaY == posObjetivoY){
			tmpDireccionF = "izquierda";
			
		}
		//Arriba
		if(posFantasmaX == posObjetivoX && posFantasmaY < posObjetivoY){
			tmpDireccionF = "arriba";
			
		}
		//Derecha
		if(posFantasmaX < posObjetivoX && posFantasmaY == posObjetivoY){
			tmpDireccionF = "derecha";
			
		}
		//Abajo
		if(posFantasmaX == posObjetivoX && posFantasmaY > posObjetivoY){
			tmpDireccionF = "abajo";
			
		}
		/*
		//caso1
		if(posFantasmaX < posObjetivoX && posFantasmaY < posObjetivoY){
			if(espacioDerecha(posFantasmaX, posFantasmaY)){
				tmpDireccionF = "derecha";
			} else{
				tmpDireccionF = "arriba";
			}
		}
		//caso2
		if(posFantasmaX > posObjetivoX && posFantasmaY < posObjetivoY){
			if(espacioIzquierda(posFantasmaX, posFantasmaY)){
				tmpDireccionF = "izquierda";
			} else{
				tmpDireccionF = "arriba";
			}
		}
		//caso3
		if(posFantasmaX < posObjetivoX && posFantasmaY > posObjetivoY){
			if(espacioDerecha(posFantasmaX, posFantasmaY)){
				tmpDireccionF = "derecha";
			} else{
				tmpDireccionF = "abajo";
			}
		}
		//caso4
		if(posFantasmaX > posObjetivoX && posFantasmaY > posObjetivoY){
			if(espacioIzquierda(posFantasmaX, posFantasmaY)){
				tmpDireccionF = "izquierda";
			} else{
				tmpDireccionF = "abajo";
			}
		}
		*/
		//Arriba
		if(direccionF.equals("arriba")){
			if(tmpDireccionF.equals("abajo")){
				if(espacioAbajo(posFantasmaX, posFantasmaY)){
					direccionF = "abajo";
				}
			}
			if(tmpDireccionF.equals("derecha")){
				if(espacioDerecha(posFantasmaX, posFantasmaY)){
					direccionF = "derecha";
				}
			}
			if(tmpDireccionF.equals("izquierda")){
				if(espacioIzquierda(posFantasmaX, posFantasmaY)){
					direccionF = "izquierda";
				}
			}
			if(direccionF.equals("arriba")){
				if(espacioArriba(posFantasmaX, posFantasmaY)){
					tmpPos[1] = tmpPos[1] + FRAMES_FANTASMA;
				}
				//pacmanTextureRegion = pacmanAnimationU.getKeyFrame(stateTime, true);
			}
		}
		//Abajo
		if(direccionF.equals("abajo")){
			if(tmpDireccionF.equals("arriba")){
				if(espacioArriba(posFantasmaX, posFantasmaY)){
					direccionF = "arriba";
				}
			}
			if(tmpDireccionF.equals("derecha")){
				if(espacioDerecha(posFantasmaX, posFantasmaY)){
					direccionF = "derecha";
				}
			}
			if(tmpDireccionF.equals("izquierda")){
				if(espacioIzquierda(posFantasmaX, posFantasmaY)){
					direccionF = "izquierda";
				}
			}
			if(direccionF.equals("abajo")){
				if(espacioAbajo(posFantasmaX, posFantasmaY)){
					tmpPos[1] = tmpPos[1] - FRAMES_FANTASMA;
				}
				//pacmanTextureRegion = pacmanAnimationD.getKeyFrame(stateTime, true);
			}
		}
		//Derecha
		if(direccionF.equals("derecha")){
			if(tmpDireccionF.equals("izquierda")){
				if(espacioIzquierda(posFantasmaX, posFantasmaY)){
					direccionF = "izquierda";
				}
			}
			if(tmpDireccionF.equals("arriba")){
				if(espacioArriba(posFantasmaX, posFantasmaY)){
					direccionF = "arriba";
				}
			}
			if(tmpDireccionF.equals("abajo")){
				if(espacioAbajo(posFantasmaX, posFantasmaY)){
					direccionF = "abajo";
				}
			}
			if(direccionF.equals("derecha")){
				if(espacioDerecha(posFantasmaX, posFantasmaY)){
					tmpPos[0] = tmpPos[0] + FRAMES_FANTASMA;
				}
				//pacmanTextureRegion = pacmanAnimationR.getKeyFrame(stateTime, true);
			}
		}
		//Izquierda
		if(direccionF.equals("izquierda")){
			if(tmpDireccionF.equals("derecha")){
				if(espacioDerecha(posFantasmaX, posFantasmaY)){
					direccionF = "derecha";
				}
			}
			if(tmpDireccionF.equals("arriba")){
				if(espacioArriba(posFantasmaX, posFantasmaY)){
					direccionF = "arriba";
				}
			}
			if(tmpDireccionF.equals("abajo")){
				if(espacioAbajo(posFantasmaX, posFantasmaY)){
					direccionF = "abajo";
				}
			}
			if(direccionF.equals("izquierda")){
				if(espacioIzquierda(posFantasmaX, posFantasmaY)){
					tmpPos[0] = tmpPos[0] - FRAMES_FANTASMA;
				}
				//pacmanTextureRegion = pacmanAnimationL.getKeyFrame(stateTime, true);
			}
		}
		return tmpPos;
	}
	
	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
		Gdx.app.log("MyLibGDXGame", "Game.create()");
		//we'll get whatever the set width is- 800x480 above, but will be the device resolution when running the android version
		SCREEN_WIDTH= Gdx.graphics.getWidth();
		SCREEN_HEIGHT= Gdx.graphics.getHeight();
 
		pacman = new Pacman(posPacmanX,posPacmanY,PIXELES,PIXELES,Color.RED);
		//setup these 3 for rendering- sprite batch will render out textures, and pixmaps allow you to draw on them
		batcher = new SpriteBatch();
		
		//Generar Laberinto-------------------
		Laberinto = new int[casillaClase.getLaberinto().length][casillaClase.getLaberinto()[0].length];
		for (int i = 0; i < Laberinto.length; i++) {
		    System.arraycopy(casillaClase.getLaberinto()[i], 0, Laberinto[i], 0, casillaClase.getLaberinto()[0].length);
		}
		laberintoOrigin = new int[casillaClase.getLaberintoMatriz().length][casillaClase.getLaberintoMatriz()[0].length];
		for (int i = 0; i < laberintoOrigin.length; i++) {
		    System.arraycopy(casillaClase.getLaberintoMatriz()[i], 0, laberintoOrigin[i], 0, casillaClase.getLaberintoMatriz()[0].length);
		}
		
		//pixmap2 = new  Pixmap(pacman.width,pacman.height, Pixmap.Format.RGBA8888);
		//texturaMov = new Texture(pixmap2);
		//drawSquare();
		//setup where we want out smiley face vector graphic to start at
		
		pacX = new BitmapFont();
		pacY = new BitmapFont();
		nxT = new BitmapFont();
		nyT = new BitmapFont();
		espacioX = new BitmapFont();
		espacioY = new BitmapFont();
		bloqueX = new BitmapFont();
		bloqueY = new BitmapFont();
		puntajeBitmap = new BitmapFont();
		
		texturaX = new Texture("llegada.png");
		spriteX = new Sprite(texturaX);
		
		numFilas = laberintoOrigin.length;
		numCol = laberintoOrigin[1].length;
		
		anchoDPad= laberintoOrigin[1].length*PIXELES;
		altoDpad = 0*PIXELES;
		
		texturaCasillas = new Texture[Laberinto.length][Laberinto[1].length];
		casillasSprite = new Sprite[Laberinto.length][Laberinto[1].length];
		
		dPadTexture = new Texture("dpad.png");
		dPadTextureU = new Texture("dpadU.png");
		dPadTextureD = new Texture("dpadD.png");
		dPadTextureL = new Texture("dpadL.png");
		dPadTextureR = new Texture("dpadR.png");
		dPadSrite = new Sprite(dPadTexture); 
		
		pacmanTexture = new Texture("PacmanSprites.png"); // #9
        TextureRegion[][] tmp = TextureRegion.split(pacmanTexture, pacmanTexture.getWidth()/FRAME_COLS, pacmanTexture.getHeight()/FRAME_ROWS);              // #10
        pacmanTextureR = new TextureRegion[FRAME_COLS];
        pacmanTextureU = new TextureRegion[FRAME_COLS];
        pacmanTextureL = new TextureRegion[FRAME_COLS];
        pacmanTextureD = new TextureRegion[FRAME_COLS];
        int index = 0;
        
        for (int j = 0; j < FRAME_COLS; j++) {
            pacmanTextureR[index] = tmp[0][j];
            pacmanTextureU[index] = tmp[1][j];
            pacmanTextureL[index] = tmp[2][j];
            pacmanTextureD[index] = tmp[3][j];
            index++;
        }
        
        pacmanAnimationR = new Animation(frameAnimacion, pacmanTextureR);      // #11
        pacmanAnimationU = new Animation(frameAnimacion, pacmanTextureU);
        pacmanAnimationL = new Animation(frameAnimacion, pacmanTextureL);
        pacmanAnimationD = new Animation(frameAnimacion, pacmanTextureD);
        //spriteBatch = new SpriteBatch();                // #12
        stateTime = 0f;                         // #13
        pacmanTextureRegion = pacmanAnimationR.getKeyFrame(stateTime, true);
        
        //Dibujo de laberinto-------------------------------------------------------------------------------------------------------
        for (int i = 0; i < Laberinto.length; i++) {
            for (int j = 0; j < Laberinto[1].length; j++) {
            	int numCasilla = Laberinto[i][j];
            	switch(numCasilla){
            	case 9:
            		texturaCasillas[i][j] = new Texture("bloqueh.png");
            		break;
            	case 12:
            		texturaCasillas[i][j] = new Texture("bloquehr.png");
            		break;
            	case 13:
            		texturaCasillas[i][j] = new Texture("bloquehl.png");
            		break;
            	case 8:
            		texturaCasillas[i][j] = new Texture("bloquev.png");
            		break;
            	case 14:
            		texturaCasillas[i][j] = new Texture("bloquevu.png");
            		break;
            	case 15:
            		texturaCasillas[i][j] = new Texture("bloquevd.png");
            		break;
            	case 99:
            		texturaCasillas[i][j] = new Texture("bloquee.png");
            		break;
            	case 16:
            		texturaCasillas[i][j] = new Texture("bloquec.png");
            		break;
            	case 1:
            		texturaCasillas[i][j] = new Texture("bloquesi.png");
            		break;
            	case 2:
            		texturaCasillas[i][j] = new Texture("bloquesd.png");
            		break;
            	case 3:
            		texturaCasillas[i][j] = new Texture("bloqueii.png");
            		break;
            	case 4:
            		texturaCasillas[i][j] = new Texture("bloqueid.png");
            		break;
            	case 0:
            		texturaCasillas[i][j] = new Texture("galleta.png");
            		break;
            	case 5:
            		texturaCasillas[i][j] = new Texture("super_galleta.png");
            		break;
            	case 10:
            		texturaCasillas[i][j] = new Texture("pacman.png");
            		break;
            	case 6:
            		texturaCasillas[i][j] = new Texture("phantom.png");
            		break;
            	case 7:
            		texturaCasillas[i][j] = new Texture("phantomAzul.png");
            		break;
            	case 17:
            		texturaCasillas[i][j] = new Texture("bloquef.png");
            		break;
            	}
        		
        		casillasSprite[i][j] = new Sprite(texturaCasillas[i][j]);
        		//casillasSprite[i][j].setOrigin(casillasSprite[i][j].getWidth()/2, casillasSprite[i][j].getHeight()/2);
        		casillasSprite[i][j].setOrigin(0, 0);
        		//casillasSprite[i][j].setPosition(j*PIXELES, (SCREEN_HEIGHT-PIXELES)-(i*PIXELES));
        		casillasSprite[i][j].setPosition(i*PIXELES, j*PIXELES);
            }
        }
        
		batcher = new SpriteBatch();
	}

	@Override
	public void show() {
		Gdx.app.log("MyLibGDXGame", "Game.create()");
		
		//we'll get whatever the set width is- 800x480 above, but will be the device resolution when running the android version
		SCREEN_WIDTH= Gdx.graphics.getWidth();
		SCREEN_HEIGHT= Gdx.graphics.getHeight();
 
		pacman = new Pacman(posPacmanX,posPacmanY,PIXELES,PIXELES,Color.RED);
		fantasmaRojo = new Fantasma(posFantasmaRoX,posFantasmaRoY,PIXELES,PIXELES,Color.RED);
		fantasmaCeleste = new Fantasma(posFantasmaCX,posFantasmaCY,PIXELES,PIXELES,Color.RED);
		fantasmaRosa = new Fantasma(posFantasmaRosaX,posFantasmaRosaY,PIXELES,PIXELES,Color.RED);
		fantasmaNaranja = new Fantasma(posFantasmaNX,posFantasmaNY,PIXELES,PIXELES,Color.RED);
		tiempoAzul = new Tiempo();
		cronoHilo = new Cronometro();
		cronoHilo.start();
		//setup these 3 for rendering- sprite batch will render out textures, and pixmaps allow you to draw on them
		batcher = new SpriteBatch();
		
		//Generar Laberinto-------------------
		Laberinto = new int[casillaClase.getLaberinto().length][casillaClase.getLaberinto()[0].length];
		for (int i = 0; i < Laberinto.length; i++) {
		    System.arraycopy(casillaClase.getLaberinto()[i], 0, Laberinto[i], 0, casillaClase.getLaberinto()[0].length);
		}
		laberintoOrigin = new int[casillaClase.getLaberintoMatriz().length][casillaClase.getLaberintoMatriz()[0].length];
		for (int i = 0; i < laberintoOrigin.length; i++) {
		    System.arraycopy(casillaClase.getLaberintoMatriz()[i], 0, laberintoOrigin[i], 0, casillaClase.getLaberintoMatriz()[0].length);
		}
		
		//pixmap2 = new  Pixmap(pacman.width,pacman.height, Pixmap.Format.RGBA8888);
		//texturaMov = new Texture(pixmap2);
		//drawSquare();
		//setup where we want out smiley face vector graphic to start at
		
		pacX = new BitmapFont();
		pacY = new BitmapFont();
		nxT = new BitmapFont();
		nyT = new BitmapFont();
		espacioX = new BitmapFont();
		espacioY = new BitmapFont();
		bloqueX = new BitmapFont();
		bloqueY = new BitmapFont();
		puntajeBitmap = new BitmapFont();
		nivelBitmap = new BitmapFont();
		vidasBitmap = new BitmapFont();
		cronometroBitmap = new BitmapFont();
		
		texturaX = new Texture("llegada.png");
		spriteX = new Sprite(texturaX);
		
		//Fantasma
		fantasmaRojoTexture = new Texture("phantomRojo.png");
		fantasmaRojoSprite = new Sprite(fantasmaRojoTexture);
		fantasmaCelesteTexture = new Texture("phantomCeleste.png");
		fantasmaCelesteSprite = new Sprite(fantasmaCelesteTexture);
		fantasmaNaranjaTexture = new Texture("phantomNaranja.png");
		fantasmaNaranjaSprite = new Sprite(fantasmaNaranjaTexture);
		fantasmaRosaTexture = new Texture("phantomRosa.png");
		fantasmaRosaSprite = new Sprite(fantasmaRosaTexture);
		
		numFilas = laberintoOrigin.length;
		numCol = laberintoOrigin[1].length;
		
		anchoDPad= laberintoOrigin[1].length*PIXELES;
		altoDpad = 0*PIXELES;
		
		//Laberinto
		texturaCasillas = new Texture[Laberinto.length][Laberinto[1].length];
		casillasSprite = new Sprite[Laberinto.length][Laberinto[1].length];
		
		//D-Pad
		dPadTexture = new Texture("dpad.png");
		dPadTextureU = new Texture("dpadU.png");
		dPadTextureD = new Texture("dpadD.png");
		dPadTextureL = new Texture("dpadL.png");
		dPadTextureR = new Texture("dpadR.png");
		dPadSrite = new Sprite(dPadTexture);
		
		//Pacman
		pacmanTexture = new Texture("PacmanSprites.png"); // #9
        TextureRegion[][] tmp = TextureRegion.split(pacmanTexture, pacmanTexture.getWidth()/FRAME_COLS, pacmanTexture.getHeight()/FRAME_ROWS);              // #10
        pacmanTextureR = new TextureRegion[FRAME_COLS];
        pacmanTextureU = new TextureRegion[FRAME_COLS];
        pacmanTextureL = new TextureRegion[FRAME_COLS];
        pacmanTextureD = new TextureRegion[FRAME_COLS];
        int index = 0;
        
        for (int j = 0; j < FRAME_COLS; j++) {
            pacmanTextureR[index] = tmp[0][j];
            pacmanTextureU[index] = tmp[1][j];
            pacmanTextureL[index] = tmp[2][j];
            pacmanTextureD[index] = tmp[3][j];
            index++;
        }
        
        pacmanAnimationR = new Animation(frameAnimacion, pacmanTextureR);      // #11
        pacmanAnimationU = new Animation(frameAnimacion, pacmanTextureU);
        pacmanAnimationL = new Animation(frameAnimacion, pacmanTextureL);
        pacmanAnimationD = new Animation(frameAnimacion, pacmanTextureD);
        //spriteBatch = new SpriteBatch();                // #12
        stateTime = 0f;                         // #13
        pacmanTextureRegion = pacmanAnimationR.getKeyFrame(stateTime, true);
        
        //Dibujo de laberinto-------------------------------------------------------------------------------------------------------
        for (int i = 0; i < Laberinto.length; i++) {
            for (int j = 0; j < Laberinto[1].length; j++) {
            	int numCasilla = Laberinto[i][j];
            	switch(numCasilla){
            	case 9:
            		texturaCasillas[i][j] = new Texture("bloqueh.png");
            		break;
            	case 12:
            		texturaCasillas[i][j] = new Texture("bloquehr.png");
            		break;
            	case 13:
            		texturaCasillas[i][j] = new Texture("bloquehl.png");
            		break;
            	case 8:
            		texturaCasillas[i][j] = new Texture("bloquev.png");
            		break;
            	case 14:
            		texturaCasillas[i][j] = new Texture("bloquevu.png");
            		break;
            	case 15:
            		texturaCasillas[i][j] = new Texture("bloquevd.png");
            		break;
            	case 99:
            		texturaCasillas[i][j] = new Texture("bloquee.png");
            		break;
            	case 16:
            		texturaCasillas[i][j] = new Texture("bloquec.png");
            		break;
            	case 1:
            		texturaCasillas[i][j] = new Texture("bloquesi.png");
            		break;
            	case 2:
            		texturaCasillas[i][j] = new Texture("bloquesd.png");
            		break;
            	case 3:
            		texturaCasillas[i][j] = new Texture("bloqueii.png");
            		break;
            	case 4:
            		texturaCasillas[i][j] = new Texture("bloqueid.png");
            		break;
            	case 0:
            		texturaCasillas[i][j] = new Texture("galleta.png");
            		contadorGalleta++;
            		break;
            	case 5:
            		texturaCasillas[i][j] = new Texture("super_galleta.png");
            		contadorGalleta++;
            		break;
            	case 10:
            		texturaCasillas[i][j] = new Texture("pacman.png");
            		break;
            	case 6:
            		texturaCasillas[i][j] = new Texture("phantom.png");
            		break;
            	case 7:
            		texturaCasillas[i][j] = new Texture("phantomAzul.png");
            		break;
            	case 17:
            		texturaCasillas[i][j] = new Texture("bloquef.png");
            		break;
            	case 11:
            		texturaCasillas[i][j] = new Texture("fruta.png");
            		break;
            	case 100:
            		texturaCasillas[i][j] = new Texture("bloquee.png");
            		break;
            	case 101:
            		texturaCasillas[i][j] = new Texture("bloquee.png");
            		break;
            	case 200:
            		texturaCasillas[i][j] = new Texture("bloquee.png");
            		break;
            	case 201:
            		texturaCasillas[i][j] = new Texture("bloquee.png");
            		break;
            	}
        		
        		casillasSprite[i][j] = new Sprite(texturaCasillas[i][j]);
        		//casillasSprite[i][j].setOrigin(casillasSprite[i][j].getWidth()/2, casillasSprite[i][j].getHeight()/2);
        		casillasSprite[i][j].setOrigin(0, 0);
        		//casillasSprite[i][j].setPosition(j*PIXELES, (SCREEN_HEIGHT-PIXELES)-(i*PIXELES));
        		casillasSprite[i][j].setPosition(i*PIXELES, j*PIXELES);
            }
        }
        
		batcher = new SpriteBatch();
	}
	
	private class Tiempo extends Thread{
		
		
		public void run(){
			while(fantasmasPowerUp){
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				contadorAzul++;
				
			}
		}
	}
	
	private class Cronometro extends Thread{
		public void run(){
			while(!gameOver){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				crono++;
				
			}
		}
	}

}
