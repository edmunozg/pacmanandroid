package com.practicaPixmap;

import com.badlogic.gdx.backends.android.AndroidApplication;

import android.app.Activity;
import android.os.Bundle;

public class JuegoActivity extends AndroidApplication {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize(new JuegoPrincipal(),true);
    }
}