package com.pacman.elementos;

/*****************************************
 ***Problema: Saliendo del Laberinto
 ***Ejemplo BFS
 ***Autor: Jhosimar George Arias Figueroa
 ******************************************/

/* Ejemplo de ingreso
//altura y ancho
8 8

.......I
.#######
.#......
.#.S...S
.###.#.#
.#...#.#
.#.###.#
........

*/

import java.util.*;

public class Laberinto {
	
	static final int MAX = 100;						//m�ximo n�mero de filas y columnas del laberinto
	static char ady[][] = new char[ MAX ][ MAX ];	//laberinto
	
	static class Estado{
	    int x , y , d;								// Fila, columna y distancie del estado
	    
	    public Estado( int x1, int y1 , int d1){
	        this.x = x1;
	        this.y = y1;
	        this.d = d1;
	    }
	};
	
	public static int BFS( int x , int y , int h , int w ){ //coordenadas de inicial "I" y dimensiones de laberinto
		
		boolean visitado[][] = new boolean[ MAX ][ MAX ];	//arreglo de estados visitados
		Queue<Estado> Q = new LinkedList<Estado>();			//Cola de todos los posibles Estados por los que se pase para llegar al destino
		Q.add( new Estado( x , y , 0 ) );					//Insertamos el estado inicial en la Cola con distnacia 0.
		
		int dx[  ] = { 0 ,  0 , 1 , -1 };		//incremento en coordenada x
		int dy[  ] = { 1 , -1 , 0 ,  0 };		//incremento en coordenada y
		int nx , ny;
		
		while( !Q.isEmpty() ){							//Mientras cola no este vacia
			Estado actual = Q.remove();					//Obtengo de la cola el estado actual, en un comienzo ser� el inicial
			if( ady[ actual.x ][ actual.y ] == 'S' ){	//Si se llego al destino (punto final)
				return actual.d;						//Retornamos distancia recorrida hasta ese momento
			}
			visitado[ actual.x ][ actual.y ] = true;	//Marco como visitado dicho estado para no volver a recorrerlo
			
			for( int i = 0 ; i < 4 ; ++i ){				//Recorremos hasta 4 porque tenemos 4 posibles adyacentes
				nx = dx[ i ] + actual.x;				//nx y ny tendran la coordenada adyacente
				ny = dy[ i ] + actual.y;				//ejemplo en i=0 y actual (3,4) -> 3+dx[0]=3+0=3, 4+dy[0]=4+1=5, nueva coordenada (3,5)
		        //aqui comprobamos que la coordenada adyacente no sobrepase las dimensiones del laberinto
		        //ademas comprobamos que no sea pared "#" y no este visitado
				if( nx >= 0 && nx < h && ny >= 0 && ny < w && !visitado[ nx ][ ny ] && ady[ nx ][ ny ] != '#' ){
					Q.add( new Estado( nx , ny , actual.d + 1 ) ); //agregamos estado adyacente aumento en 1 la distancia recorrida
				}
			}
		}
		return -1;
	}
	

	public static void main(String[] args) {
		
		int h , w , x = 0 , y = 0;
		Scanner sc = new Scanner( System.in );
		System.out.println( "Ingrese altura del laberinto: " );
		h = sc.nextInt(); 
		System.out.println( "Ingrese ancho del laberinto: " );
		w = sc.nextInt();
		System.out.println("Ingrese el laberinto, con un solo valor inicial I, valor final sera S: ");
		String line = sc.nextLine();			//funciona igual que getline o gets de c++
		for (int i = 0; i < h ; ++i ) {
			line = sc.nextLine();
			for( int j = 0 ; j < w ; ++j ){
				ady[ i ][ j ] = line.charAt( j );
			    if( ady[ i ][ j ] == 'I' ){		 //obtengo coordenada de valor inicial
			    	x = i; y = j;
			    }
			}
		}
		
		int min = BFS( x , y , h , w );
		if( min == -1 ) System.out.println("No se pudo llegar al destino");
		else System.out.println( "Menor numero de pasos: " + min );
		
	}

}
