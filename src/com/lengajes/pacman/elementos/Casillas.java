package com.lengajes.pacman.elementos;

import com.badlogic.gdx.utils.Array;

public class Casillas {
	
	private int nivel;
	
	private int[][] laberinto1= {
			{1 , 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 2},
			{8 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99, 8},
			{8 ,99, 1, 9,12,99, 1, 9, 9, 9, 9, 2,99,13, 9, 2,99, 8},
			{8 ,99, 8,99,99,99, 8,99,99,99,99, 8,99,99,99, 8,99, 8},
			{8 ,99, 8,99,14,99, 3,12,17,17,13, 4,99,14,99, 8,99, 8},
			{15,99,15,99,15,99,99,99,99,99,99,99,99,15,99,15,99,15},
			{99,99,99,99,99,99,13, 9, 9, 9, 9,12,99,99,99,99,99,99},
			{14,99, 1, 9,12,99,99,99,99,99,99,99,99,13, 9, 2,99,14},
			{8 ,99,15,99,99,99, 1, 9, 9, 9, 9, 2,99,99,99,15,99, 8},
			{8 ,99,99,99,16,99,15,99,99,99,99,15,99,16,99,99,99, 8},
			{8 ,99,14,99,99,99,99,99, 1, 2,99,99,99,99,99, 14,99, 8},
			{8 ,99, 3, 9,12,99,13, 9, 4, 3, 9,12,99,13, 9, 4,99, 8},
			{8 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99, 8},
			{3 , 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 4}
			};
	
	private int[][] laberinto2= {
			{1, 9, 9, 2},
			{3, 8, 8, 4}
			};
	
	private static final int BLOCK_H = 9;
	private static final int BLOCK_V = 8;
	private static final int BLOCK_SI = 1;
	private static final int BLOCK_SD = 2;
	private static final int BLOCK_II = 3;
	private static final int BLOCK_ID = 4;
	private static final int BLOCK_HR = 12;
	private static final int BLOCK_HL = 13;
	private static final int BLOCK_VU = 14;
	private static final int BLOCK_VD = 15;
	private static final int BLOCK_C = 16;
	private static final int BLOCK_E = 99;
	private static final int SNAKE = 10;
	private static final int PELLET = 0;
	private static final int MONSTER = 6;
	private static final int SCARED_MONSTER = 7;
	private static final int SUPER_PELLET = 5;
	private static final int FRUTA = 11;

	
	private int[][] ordenarTablero(int [][] lab){
		
		int[][] tmpTablero = new int[lab[0].length][lab.length];
		int tamLabF = lab.length-1;
		int tamLabC = lab[0].length-1;
		for(int i=tmpTablero.length-1; i>= 0; i--){
			for(int j=0; j<tmpTablero[0].length; j++){
				tmpTablero[i][j] = lab[j][tamLabC];
			}
			tamLabC--;
		}
		
		int[][] tmpTableroFinal = new int[tmpTablero.length][tmpTablero[0].length];
		int tamTmpF = tmpTablero.length-1;
		int tamTmpC = tmpTablero[0].length-1;
		for(int i=0; i< tmpTableroFinal.length; i++){
			for(int j=0; j<tmpTableroFinal[0].length; j++){
				tmpTableroFinal[i][j] = tmpTablero[i][tamTmpC--];
			}
			tamTmpC = tmpTablero[0].length-1;
		}
		
		return tmpTableroFinal;
		
	}
	
	public Casillas(int nivel){
		this.nivel = nivel;
	}
	
	public int[][] getLaberinto(){
		
		if(nivel == 1){
			return ordenarTablero(laberinto1);
		}
		
		
		return null;
	}
	public int[][] getLaberintoMatriz(){
		
		return laberinto1;
	}
	
}
