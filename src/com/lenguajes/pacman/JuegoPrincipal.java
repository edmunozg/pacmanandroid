package com.lenguajes.pacman;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;

public class JuegoPrincipal extends Game implements ApplicationListener{

	@Override
	public void create() {
		this.setScreen(new PantallaJuego(this));
		
	}

}
