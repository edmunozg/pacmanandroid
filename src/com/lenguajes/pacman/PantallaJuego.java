package com.lenguajes.pacman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import android.text.TextWatcher;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.lengajes.pacman.elementos.Casillas;
import com.lengajes.pacman.elementos.Pacman;



public class PantallaJuego implements Screen {
		private JuegoPrincipal juego;
	//private Texture texturaCasilla, texturaPacman;
	//private Sprite casillaSprite, pacmanSprite;
	private SpriteBatch batcher;
	private static final int        PIXELES = 32;
	private static final int        FRAME_COLS = 4;         // #1
    private static final int        FRAME_ROWS = 4;         // #2
    private static final int 		posPacmanX = 7*PIXELES;
    private static final int 		posPacmanY = 6*PIXELES;
    private static final float		frameAnimacion = 0.05f;

    private Animation                       walkAnimationR;          // #3
    private Animation                       walkAnimationU;
    private Animation                       walkAnimationL;
    private Animation                       walkAnimationD;
    private Texture                         walkSheet;              // #4
    private TextureRegion[]                 pacmanTextureR;
    private TextureRegion[]                 pacmanTextureU;
    private TextureRegion[]                 pacmanTextureL;
    private TextureRegion[]                 pacmanTextureD;			// #5
    private TextureRegion                   currentFrame;           // #7
    private Texture dPadTexture;
    private Sprite dPadSrite;
    private int anchoDPad=0, altoDpad=0;
    private String direccion="derecha", tmpDireccion ="";
    
    private float stateTime;                                        // #8
    
    //CASILLAS
    private Casillas casillaClase = new Casillas(1);
    private int[][] Laberinto;
    private int[][] laberintoOrigin;
    private int[] tmpPosiciones = new int[2]; //guarda posiciones devuelta por el metodo de mover personajes
    private Texture[][] texturaCasillas;
    private Sprite[][] casillasSprite;
    private int numCol, numFilas;
    private static int SCREEN_WIDTH = 800;
    private static int SCREEN_HEIGHT = 480;
    private static int FRAMES = 4;
 
	//things needed to draw
    //private Pixmap pixmap2;
    //private Texture texturaMov;
    private Pacman pacman;
    private boolean cumpleX = false, cumpleY = false;
    private int casoxy = 0;
    private int ny=-1;
	private int nx=-1;
	private int posYpacman, posXpacman;
	private Texture texturaX;
	private Sprite spriteX;
	
	private BitmapFont pacX;
	private BitmapFont pacY;
	private BitmapFont nxT;
	private BitmapFont nyT;
	private BitmapFont espacioX;
	private BitmapFont espacioY;
	private BitmapFont bloqueX;
	private BitmapFont bloqueY;
	
	public PantallaJuego(JuegoPrincipal game) {
		this.juego = game;
	}
	@Override
	public void dispose() {
		Gdx.app.log("MyLibGDXGame", "Game.dispose()");
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		walkSheet.dispose();
		for (int i = 0; i < numFilas; i++) {
	        for (int j = 0; j < numCol; j++) {
	        	texturaCasillas[i][j].dispose();
	        }
		}
		Gdx.app.log("MyLibGDXGame", "Game.pause()");
		//texturaMov.dispose();
		//pixmap2.dispose();
		pacX.dispose();
		pacY.dispose();
		nxT.dispose();
		nyT.dispose();
		dPadTexture.dispose();
		
		batcher.dispose();
	}

	@Override
	public void render(float arg0) {
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT | GL10.GL_STENCIL_BUFFER_BIT);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT); 
		
		stateTime += Gdx.graphics.getDeltaTime();
		
		batcher.begin();
		
		//MOVER TOTALMENTE CON EL TOUCH
		if(Gdx.input.isTouched())
		{
			nx = Gdx.input.getX();
			//nx = nx*32;
			ny = (SCREEN_HEIGHT - Gdx.input.getY());
			//ny = ny*32;
			
			cumpleX = true;
			cumpleY = false;

			
		}
		if(nx != -1 && ny != -1){
			if(pacman.getX() < nx && pacman.getY() < ny){
				casoxy = 1;
			}
			if(pacman.getX() < nx && pacman.getY() > ny){
				casoxy = 2;
			}
			if(pacman.getX() > nx && pacman.getY() < ny){
				casoxy = 3;
			}
			if(pacman.getX() > nx && pacman.getY() > ny){
				casoxy = 4;
			}
		}
		
		tmpPosiciones = movimientoPersonajeDPad(pacman.getX(), pacman.getY());
		pacman.setX(tmpPosiciones[0]);
		pacman.setY(tmpPosiciones[1]);
		
		pacX.draw(batcher, "pacX: "+pacman.getX(), SCREEN_WIDTH-100, 300);
		pacY.draw(batcher, "pacY: "+pacman.getY(), SCREEN_WIDTH-100, 275);
		nxT.draw(batcher, "nx: "+nx, SCREEN_WIDTH-100, 250);
		nyT.draw(batcher, "ny: "+ny, SCREEN_WIDTH-100, 225);
		/*espacioX.draw(batcher, ""+pacman.getX(), 125, 160);
		espacioY.draw(batcher, ""+pacman.getX(), 150, 160);
		bloqueX.draw(batcher, ""+pacman.getX(), 175, 160);
		bloqueY.draw(batcher, ""+pacman.getX(), 200, 160);
		*/
		batcher.draw(dPadSrite, anchoDPad,altoDpad);
		batcher.draw(spriteX, nx, ny,pacman.getWidth(),pacman.getHeight());
		batcher.draw(currentFrame, pacman.getX(), pacman.getY(),pacman.getWidth(),pacman.getHeight());
		
		
        for (int i = 0; i < Laberinto.length; i++) {
            for (int j = 0; j < Laberinto[i].length; j++) {
            	casillasSprite[i][j].draw(batcher);
            	
            }
        }
        
        
        
        
		batcher.end();
	}
	
	private int[] movimientoPersonajeDPad(int posX, int posY){
		int []tmpPos = {posX,posY};
		//Izquierda
		if(a(0)<nx && a(2)>nx && b(2)<ny && b(4)>ny){
			tmpDireccion = "izquierda";
		}
		//Arriba
		if(a(2)<nx && a(4)>nx && b(4)<ny && b(6)>ny){
			tmpDireccion = "arriba";
		}
		//Derecha
		if(a(4)<nx && a(6)>nx && b(2)<ny && b(4)>ny){
			tmpDireccion = "derecha";
		}
		//Abajo
		if(a(2)<nx && a(4)>nx && b(0)<ny && b(2)>ny){
			tmpDireccion = "abajo";
		}
		//Arriba
		if(direccion.equals("arriba")){
			if(a(2)<nx && a(4)>nx && b(0)<ny && b(2)>ny){
				direccion = "abajo";
			}
			if(tmpDireccion.equals("derecha")){
				if(espacioDerecha(posX, posY)){
					direccion = "derecha";
				}
			}
			if(tmpDireccion.equals("izquierda")){
				if(espacioIzquierda(posX, posY)){
					direccion = "izquierda";
				}
			}
			if(direccion.equals("arriba")){
				if(espacioArriba(posX, posY)){
					tmpPos[1] = tmpPos[1] + FRAMES;
				}
				currentFrame = walkAnimationU.getKeyFrame(stateTime, true);
			}
		}
		//Abajo
		if(direccion.equals("abajo")){
			if(a(2)<nx && a(4)>nx && b(4)<ny && b(6)>ny){
				direccion = "arriba";
			}
			if(tmpDireccion.equals("derecha")){
				if(espacioDerecha(posX, posY)){
					direccion = "derecha";
				}
			}
			if(tmpDireccion.equals("izquierda")){
				if(espacioIzquierda(posX, posY)){
					direccion = "izquierda";
				}
			}
			if(direccion.equals("abajo")){
				if(espacioAbajo(posX, posY)){
					tmpPos[1] = tmpPos[1] - FRAMES;
				}
				currentFrame = walkAnimationD.getKeyFrame(stateTime, true);
			}
		}
		//Derecha
		if(direccion.equals("derecha")){
			if(a(0)<nx && a(2)>nx && b(2)<ny && b(4)>ny){
				direccion = "izquierda";
			}
			if(tmpDireccion.equals("arriba")){
				if(espacioArriba(posX, posY)){
					direccion = "arriba";
				}
			}
			if(tmpDireccion.equals("abajo")){
				if(espacioAbajo(posX, posY)){
					direccion = "abajo";
				}
			}
			if(direccion.equals("derecha")){
				if(espacioDerecha(posX, posY)){
					tmpPos[0] = tmpPos[0] + FRAMES;
				}
				currentFrame = walkAnimationR.getKeyFrame(stateTime, true);
			}
		}
		//Izquierda
		if(direccion.equals("izquierda")){
			if(a(4)<nx && a(6)>nx && b(2)<ny && b(4)>ny){
				direccion = "derecha";
			}
			if(tmpDireccion.equals("arriba")){
				if(espacioArriba(posX, posY)){
					direccion = "arriba";
				}
			}
			if(tmpDireccion.equals("abajo")){
				if(espacioAbajo(posX, posY)){
					direccion = "abajo";
				}
			}
			if(direccion.equals("izquierda")){
				if(espacioIzquierda(posX, posY)){
					tmpPos[0] = tmpPos[0] - FRAMES;
				}
				currentFrame = walkAnimationL.getKeyFrame(stateTime, true);
			}
		}
		return tmpPos;
	}
	private int a(int n){
		return anchoDPad+PIXELES*n;
	}
	private int b(int n){
		return altoDpad+PIXELES*n;
	}
	
	private boolean espacioDerecha(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-(posY / PIXELES)-1;
		cj = ((posX / PIXELES)+1);
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES);
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( tmpBloqueY>=posY && posY>=(tmpBloqueY-PIXELES))
					//&& tmpBloqueY>=(posY+PIXELES) && (posY+PIXELES)>=(tmpBloqueY-PIXELES))
				hayEspacio = true;
		}
		
		return hayEspacio;
	}
	private boolean espacioIzquierda(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-(posY / PIXELES)-1;
		cj = ((posX-FRAMES) / PIXELES);
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES);
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( tmpBloqueY>=posY && posY>=(tmpBloqueY-PIXELES))
					//&& tmpBloqueY>=(posY+PIXELES) && (posY+PIXELES)>=(tmpBloqueY-PIXELES))
				hayEspacio = true;
		}
		
		return hayEspacio;
	}
	private boolean espacioArriba(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-(posY / PIXELES)-1-1;
		cj = ((posX / PIXELES));
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES);
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( (tmpBloqueX)<=posX && posX<=tmpBloqueX+PIXELES
				&& (tmpBloqueX)<=(posX+32) && (posX+32)<=tmpBloqueX+PIXELES)
				hayEspacio = true;
		}
		
		return hayEspacio;
	}
	private boolean espacioAbajo(int posX, int posY){
		boolean hayEspacio = false;
		int ci;
		int cj;
		int tmpBloqueMatriz, tmpBloqueX, tmpBloqueY;
		ci = laberintoOrigin.length-((posY-FRAMES) / PIXELES)-1;
		cj = (posX / PIXELES);
		tmpBloqueMatriz = laberintoOrigin[ci][cj];
		tmpBloqueX = cj*32;
		tmpBloqueY = SCREEN_HEIGHT-((ci+1)*PIXELES);
		
		if((tmpBloqueMatriz == 99 || tmpBloqueMatriz == 0 || tmpBloqueMatriz == 5 || tmpBloqueMatriz == 11)){
			//int posNewY = SCREEN_HEIGHT-posY-PIXELES;
			if( (tmpBloqueX)<=posX && posX<=tmpBloqueX+PIXELES
				&& (tmpBloqueX)<=(posX+32) && (posX+32)<=tmpBloqueX+PIXELES)
				hayEspacio = true;
		}
		
		return hayEspacio;
	}
	
	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
		batcher = new SpriteBatch();
		//pixmap2 = new  Pixmap(pacman.width,pacman.height, Pixmap.Format.RGBA8888);
		//texturaMov = new Texture(pixmap2);
		//drawSquare();
		//setup where we want out smiley face vector graphic to start at
		pacX = new BitmapFont();
		pacY = new BitmapFont();
		nxT = new BitmapFont();
		nyT = new BitmapFont();
		espacioX = new BitmapFont();
		espacioY = new BitmapFont();
		bloqueX = new BitmapFont();
		bloqueY = new BitmapFont();
		
		Gdx.app.log("MyLibGDXGame", "Game.resume()");
		pacX= new BitmapFont();
		texturaX = new Texture("llegada.png");
		spriteX = new Sprite(texturaX);
		
		texturaCasillas = new Texture[Laberinto.length][Laberinto[1].length];
		casillasSprite = new Sprite[Laberinto.length][Laberinto[1].length];

		dPadTexture = new Texture("dPad.png");
		dPadSrite = new Sprite(dPadTexture); 
		
		walkSheet = new Texture("PacmanSprites.png"); // #9
        TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth()/FRAME_COLS, walkSheet.getHeight()/FRAME_ROWS);              // #10
        pacmanTextureR = new TextureRegion[FRAME_COLS];
        pacmanTextureU = new TextureRegion[FRAME_COLS];
        pacmanTextureL = new TextureRegion[FRAME_COLS];
        pacmanTextureD = new TextureRegion[FRAME_COLS];
        int index = 0;
        
        for (int j = 0; j < FRAME_COLS; j++) {
            pacmanTextureR[index] = tmp[0][j];
            pacmanTextureU[index] = tmp[1][j];
            pacmanTextureL[index] = tmp[2][j];
            pacmanTextureD[index] = tmp[3][j];
            index++;
        }
        
        walkAnimationR = new Animation(frameAnimacion, pacmanTextureR);      // #11
        walkAnimationU = new Animation(frameAnimacion, pacmanTextureU);
        walkAnimationL = new Animation(frameAnimacion, pacmanTextureL);
        walkAnimationD = new Animation(frameAnimacion, pacmanTextureD);
        //spriteBatch = new SpriteBatch();                // #12
        stateTime = 0f;                              // #13
        currentFrame = walkAnimationR.getKeyFrame(stateTime, true);
        
        //Dibujo de laberinto-------------------------------------------------------------------------------------------------------
        for (int i = 0; i < Laberinto.length; i++) {
            for (int j = 0; j < Laberinto[1].length; j++) {
            	int numCasilla = Laberinto[i][j];
            	switch(numCasilla){
	            	case 9:
	            		texturaCasillas[i][j] = new Texture("bloqueh.png");
	            		break;
	            	case 12:
	            		texturaCasillas[i][j] = new Texture("bloquehr.png");
	            		break;
	            	case 13:
	            		texturaCasillas[i][j] = new Texture("bloquehl.png");
	            		break;
	            	case 8:
	            		texturaCasillas[i][j] = new Texture("bloquev.png");
	            		break;
	            	case 14:
	            		texturaCasillas[i][j] = new Texture("bloquevu.png");
	            		break;
	            	case 15:
	            		texturaCasillas[i][j] = new Texture("bloquevd.png");
	            		break;
	            	case 99:
	            		texturaCasillas[i][j] = new Texture("bloquee.png");
	            		break;
	            	case 16:
	            		texturaCasillas[i][j] = new Texture("bloquec.png");
	            		break;
	            	case 1:
	            		texturaCasillas[i][j] = new Texture("bloquesi.png");
	            		break;
	            	case 2:
	            		texturaCasillas[i][j] = new Texture("bloquesd.png");
	            		break;
	            	case 3:
	            		texturaCasillas[i][j] = new Texture("bloqueii.png");
	            		break;
	            	case 4:
	            		texturaCasillas[i][j] = new Texture("bloqueid.png");
	            		break;
	            	case 0:
	            		texturaCasillas[i][j] = new Texture("galleta.png");
	            		break;
	            	case 5:
	            		texturaCasillas[i][j] = new Texture("super_galleta.png");
	            		break;
	            	case 10:
	            		texturaCasillas[i][j] = new Texture("pacman.png");
	            		break;
	            	case 6:
	            		texturaCasillas[i][j] = new Texture("phantom.png");
	            		break;
	            	case 7:
	            		texturaCasillas[i][j] = new Texture("phantomAzul.png");
	            		break;
	            	case 17:
	            		texturaCasillas[i][j] = new Texture("bloquef.png");
	            		break;
            	}
        		
        		casillasSprite[i][j] = new Sprite(texturaCasillas[i][j]);
        		//casillasSprite[i][j].setOrigin(casillasSprite[i][j].getWidth()/2, casillasSprite[i][j].getHeight()/2);
        		casillasSprite[i][j].setOrigin(0, 0);
        		//casillasSprite[i][j].setPosition(j*PIXELES, (SCREEN_HEIGHT-PIXELES)-(i*PIXELES));
        		casillasSprite[i][j].setPosition(i*PIXELES, j*PIXELES);
            }
        }
        
		batcher = new SpriteBatch();
	}

	@Override
	public void show() {
		Gdx.app.log("MyLibGDXGame", "Game.create()");
		//we'll get whatever the set width is- 800x480 above, but will be the device resolution when running the android version
		SCREEN_WIDTH= Gdx.graphics.getWidth();
		SCREEN_HEIGHT= Gdx.graphics.getHeight();
 
		pacman = new Pacman(posPacmanX,posPacmanY,PIXELES,PIXELES,Color.RED);
		//setup these 3 for rendering- sprite batch will render out textures, and pixmaps allow you to draw on them
		batcher = new SpriteBatch();
		
		//Generar Laberinto-------------------
		Laberinto = new int[casillaClase.getLaberinto().length][casillaClase.getLaberinto()[0].length];
		for (int i = 0; i < Laberinto.length; i++) {
		    System.arraycopy(casillaClase.getLaberinto()[i], 0, Laberinto[i], 0, casillaClase.getLaberinto()[0].length);
		}
		laberintoOrigin = new int[casillaClase.getLaberintoMatriz().length][casillaClase.getLaberintoMatriz()[0].length];
		for (int i = 0; i < laberintoOrigin.length; i++) {
		    System.arraycopy(casillaClase.getLaberintoMatriz()[i], 0, laberintoOrigin[i], 0, casillaClase.getLaberintoMatriz()[0].length);
		}
		
		//pixmap2 = new  Pixmap(pacman.width,pacman.height, Pixmap.Format.RGBA8888);
		//texturaMov = new Texture(pixmap2);
		//drawSquare();
		//setup where we want out smiley face vector graphic to start at
		
		pacX = new BitmapFont();
		pacY = new BitmapFont();
		nxT = new BitmapFont();
		nyT = new BitmapFont();
		espacioX = new BitmapFont();
		espacioY = new BitmapFont();
		bloqueX = new BitmapFont();
		bloqueY = new BitmapFont();
		
		texturaX = new Texture("llegada.png");
		spriteX = new Sprite(texturaX);
		
		numFilas = laberintoOrigin.length;
		numCol = laberintoOrigin[1].length;
		
		anchoDPad= laberintoOrigin[1].length*PIXELES;
		altoDpad = 0*PIXELES;
		
		texturaCasillas = new Texture[Laberinto.length][Laberinto[1].length];
		casillasSprite = new Sprite[Laberinto.length][Laberinto[1].length];
		
		dPadTexture = new Texture("dPad.png");
		dPadSrite = new Sprite(dPadTexture); 
		
		walkSheet = new Texture("PacmanSprites.png"); // #9
        TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth()/FRAME_COLS, walkSheet.getHeight()/FRAME_ROWS);              // #10
        pacmanTextureR = new TextureRegion[FRAME_COLS];
        pacmanTextureU = new TextureRegion[FRAME_COLS];
        pacmanTextureL = new TextureRegion[FRAME_COLS];
        pacmanTextureD = new TextureRegion[FRAME_COLS];
        int index = 0;
        
        for (int j = 0; j < FRAME_COLS; j++) {
            pacmanTextureR[index] = tmp[0][j];
            pacmanTextureU[index] = tmp[1][j];
            pacmanTextureL[index] = tmp[2][j];
            pacmanTextureD[index] = tmp[3][j];
            index++;
        }
        
        walkAnimationR = new Animation(frameAnimacion, pacmanTextureR);      // #11
        walkAnimationU = new Animation(frameAnimacion, pacmanTextureU);
        walkAnimationL = new Animation(frameAnimacion, pacmanTextureL);
        walkAnimationD = new Animation(frameAnimacion, pacmanTextureD);
        //spriteBatch = new SpriteBatch();                // #12
        stateTime = 0f;                         // #13
        currentFrame = walkAnimationR.getKeyFrame(stateTime, true);
        
        //Dibujo de laberinto-------------------------------------------------------------------------------------------------------
        for (int i = 0; i < Laberinto.length; i++) {
            for (int j = 0; j < Laberinto[1].length; j++) {
            	int numCasilla = Laberinto[i][j];
            	switch(numCasilla){
            	case 9:
            		texturaCasillas[i][j] = new Texture("bloqueh.png");
            		break;
            	case 12:
            		texturaCasillas[i][j] = new Texture("bloquehr.png");
            		break;
            	case 13:
            		texturaCasillas[i][j] = new Texture("bloquehl.png");
            		break;
            	case 8:
            		texturaCasillas[i][j] = new Texture("bloquev.png");
            		break;
            	case 14:
            		texturaCasillas[i][j] = new Texture("bloquevu.png");
            		break;
            	case 15:
            		texturaCasillas[i][j] = new Texture("bloquevd.png");
            		break;
            	case 99:
            		texturaCasillas[i][j] = new Texture("bloquee.png");
            		break;
            	case 16:
            		texturaCasillas[i][j] = new Texture("bloquec.png");
            		break;
            	case 1:
            		texturaCasillas[i][j] = new Texture("bloquesi.png");
            		break;
            	case 2:
            		texturaCasillas[i][j] = new Texture("bloquesd.png");
            		break;
            	case 3:
            		texturaCasillas[i][j] = new Texture("bloqueii.png");
            		break;
            	case 4:
            		texturaCasillas[i][j] = new Texture("bloqueid.png");
            		break;
            	case 0:
            		texturaCasillas[i][j] = new Texture("galleta.png");
            		break;
            	case 5:
            		texturaCasillas[i][j] = new Texture("super_galleta.png");
            		break;
            	case 10:
            		texturaCasillas[i][j] = new Texture("pacman.png");
            		break;
            	case 6:
            		texturaCasillas[i][j] = new Texture("phantom.png");
            		break;
            	case 7:
            		texturaCasillas[i][j] = new Texture("phantomAzul.png");
            		break;
            	case 17:
            		texturaCasillas[i][j] = new Texture("bloquef.png");
            		break;
            	}
        		
        		casillasSprite[i][j] = new Sprite(texturaCasillas[i][j]);
        		//casillasSprite[i][j].setOrigin(casillasSprite[i][j].getWidth()/2, casillasSprite[i][j].getHeight()/2);
        		casillasSprite[i][j].setOrigin(0, 0);
        		//casillasSprite[i][j].setPosition(j*PIXELES, (SCREEN_HEIGHT-PIXELES)-(i*PIXELES));
        		casillasSprite[i][j].setPosition(i*PIXELES, j*PIXELES);
            }
        }
        
		batcher = new SpriteBatch();
	}
	
	public void drawSquare(){
		//pixmap2.setColor(pacman.color);
		//pixmap2.fillRectangle(0, 0, pacman.width,pacman.height);
 
 
		//texturaMov.draw(pixmap2, 0, 0);
		//texturaMov.bind();
	}

}
