package com.lenguajes.pacman;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Vista implements ApplicationListener{

	Pixmap pixmap;
	Texture textura;
	SpriteBatch batcher;
	
	
	@Override
	public void create() {
		
		pixmap = new Pixmap(200,200,Pixmap.Format.RGB888);
		pixmap.setColor(244,21,123,123);
		//pixmap.fillRectangle(0, 0, 200, 200);
		pixmap.drawPixel(2, 2);
		textura = new Texture(pixmap);
		textura.bind();
		
		batcher = new SpriteBatch();
		
	}

	@Override
	public void dispose() {

		batcher.dispose();
		textura.dispose();
		pixmap.dispose();
		
	}

	@Override
	public void pause() {
	
		batcher.dispose();
		textura.dispose();
		pixmap.dispose();
		
	}

	@Override
	public void render() 
	{
		
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT | GL10.GL_STENCIL_BUFFER_BIT);
		
		batcher.begin();
		batcher.draw(textura,50,100);
		batcher.draw(textura, 0, 0);
		batcher.draw(textura, 100,200);
		batcher.end();
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		pixmap = new Pixmap(200,200,Pixmap.Format.RGB888);
		pixmap.setColor(Color.BLUE);
		pixmap.fillRectangle(0, 0, 100, 100);
		
		textura = new Texture(pixmap);
		textura.bind();
		
		batcher = new SpriteBatch();
		
	}

}
